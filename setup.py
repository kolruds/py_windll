#!/usr/bin/env python
import os
from setuptools import *

# read long description
with open(os.path.join(os.path.dirname(__file__), 'readme.md')) as f:
    long_description = f.read()

setup(name='py_windll',
      version='0.1',
      description='A repository of python wrapper functions for ctypes windll functions.',
      long_description=long_description,
      author='kolruds',
      author_email='stiankolrud@gmail.com',
      url='https://gitlab.com/kolruds/py_windll',
      packages=find_packages())
