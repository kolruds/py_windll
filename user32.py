import ctypes

from TYPES import *


def GetWindowThreadProcessId(hwnd):
    """ Get Process ID (pid) from window handle (hwnd).

    Args:
        hwnd (int): hwnd window handle

    Returns:
        int: Process ID
    """
    _hwnd = ctypes.c_long(hwnd)
    _proc_id = ctypes.c_ulong()

    ctypes.windll.user32.GetWindowThreadProcessId(
        _hwnd,
        ctypes.byref(_proc_id)
    )

    return _proc_id.value
