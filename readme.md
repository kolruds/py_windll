py_windll - Python Wrappers for ctypes.windll
=============================================

Usage
-----
The functions and types are named as in the dlls, types CAPITALIZED and functions FirstLetterCapitalized.

Call the functions as you would with any other python functions, no ctypes required. For special cases, you could use the builtin TYPES provided.

**Example**
``` python
process = py_windll.OpenProcess(desired_access=py_windll.PROCESS_ALL_ACCESS,
								process_id=1000)
```

Instead of giving a constant value to desired_access, this is provided in py_windll. In this case, this is the default value, this function really only needs the process_id argument.

Installation
------------
Clone the repository to a desired location

`git clone https://gitlab.com/kolruds/py_windll.git`

or install with pip

`pip install git+https://gitlab.com/kolruds/py_windll`
