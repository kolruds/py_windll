import ctypes

from TYPES import *


def OpenProcess(desired_access=PROCESS_ALL_ACCESS,
                inherit_handle=True,
                process_id=-1):
    """ Open an existing local process.

    Args:
        desired_access (TYPE): Desired access level. Default PROCESS_ALL_ACCESS
        inherit_handle (bool): If true, processes created by this process
            will inherit this handle. Default True
        process_id      (int): Local process identifier

    """
    res = ctypes.windll.kernel32.OpenProcess(
        desired_access,
        inherit_handle,
        process_id
    )

    return res


def GetProcessTimes(handle):
    """ Retrieves timing information for the specified process.

    Args:
        handle (process): A handle to the process.

    Returns:
        dict { CREATION: int,
               EXIT: int,
               KERNEL: int,
               USER: int}
            Return values are 64bit ints, and do not represent timestamp as is.
    """
    _creation = FILETIME()
    _exit = FILETIME()
    _kernel = FILETIME()
    _user = FILETIME()

    ctypes.windll.kernel32.GetProcessTimes(
        handle,
        ctypes.byref(_creation),
        ctypes.byref(_exit),
        ctypes.byref(_kernel),
        ctypes.byref(_user)
    )

    return {'CREATION': _creation,
            'EXIT':     _exit,
            'KERNEL':   _kernel,
            'USER':     _user}


def GetSystemTime():
    """ Retrieves the current system date and time (UTC).

    Returns:
        SYSTEMTIME: ctypes structure.
    """
    _sys_time = SYSTEMTIME()
    ctypes.windll.kernel32.GetSystemTime(ctypes.byref(_sys_time))
    return _sys_time
