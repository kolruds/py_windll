import ctypes

from wintypes import *

"""
TYPES is a copy of cpython/wintypes:
https://github.com/python/cpython/blob/master/Lib/ctypes/wintypes.py

At the moment, structures reside here. May move to different files.
"""

PROCESS_ALL_ACCESS = (0x000F0000L | 0x00100000L | 0xFFF)

class FILETIME(ctypes.Structure):
    _fields_ = [('dwLowDateTime', DWORD),
                ('dwHighDateTime', DWORD)]

class SYSTEMTIME(ctypes.Structure):
    _fields_ = [('wYear', WORD),
                ('wMonth', WORD),
                ('wDayOffWeek', WORD),
                ('wDay', WORD),
                ('wHour', WORD),
                ('wMinute', WORD),
                ('wSecond', WORD),
                ('wMilliseconds', WORD)]

class DYNAMIC_TIME_ZONE_INFORMATION(ctypes.Structure):
    _fields_ = [('Bias', LONG),
                ('StandardName', WCHAR),
                ('StandardDate', SYSTEMTIME),
                ('StandardBias', LONG),
                ('DaylightName', WCHAR),
                ('DaylightDate', SYSTEMTIME),
                ('DaylightBias', LONG),
                ('TimeZoneKeyName', WCHAR),
                ('DynamicDaylightTimeDisabled', BOOLEAN)]

class TIME_ZONE_INFORMATION(ctypes.Structure):
    _fields_ = [('Bias', LONG),
                ('StandardName', WCHAR),
                ('StandardDate', SYSTEMTIME),
                ('StandardBias', LONG),
                ('DaylightName', WCHAR),
                ('DaylightDate', SYSTEMTIME),
                ('DaylightBias', LONG)]
